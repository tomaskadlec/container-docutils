container-docutils
==================

.. _`run.sh`: run.sh
.. |run.sh| replace:: ``run.sh``   

This project builds an Docker image that contains `docutils`_. Image is based on
`Alpine`_ image.

Installation
------------

#. Download or clone the project, e.g. ::

     git clone https://tomaskadlec@gitlab.com/tomaskadlec/container-docutils.git /usr/local/container/docutils
   
#. Add |run.sh|_ script to ``PATH``, e.g. symlink it to a directory already in
   ``PATH``::

     ln -s /usr/local/container/docutils/run.sh /usr/local/bin/docutils-container

Usage
-----

Just run |run.sh|_ (or whatever it is called in ``PATH``). It will start a
container change identity to current user and change to current working
directory. The container will be removed if it is stopped.

License
-------

.. _`The MIT License`: LICENSE

This project is licensed under `The MIT License`_.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
